$(document).ready(function() {
    $("#taskform").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'index.php',
            data: $(this).serialize(),
            success: function(response) {
                var jsonData = JSON.parse(response)
                console.log(jsonData);
                alert("Task has been succesfully inserted!");
                $.get('tasks.txt', function(data) {
                    var task = "";
                    $.each(data.split(/[\n\r]+/), function(index, value) {
                        task += '<li>' + value + '</li>'
                    });
                    $("#task").html(task);
                });
                console.log(typeof(jsonData));
            }
        })
    })

    $("#clearSession").on('click', function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: 'clearsession.php',
            success: function(response) {
                var jsonData = JSON.parse(response)
                console.log(jsonData);
            }
        })
    })
})